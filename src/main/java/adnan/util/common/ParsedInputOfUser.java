package adnan.util.common;

public class ParsedInputOfUser {

	private String destinationIp;
	private int nVirtualMachine; // nVM only used for printing
	private int nMessage;
	private int thinkTime;
	private String inputStringFileName;
	
	/* the threadName is used when this file is run as multiple 
	 * processes from Linux. The threadName is used to distinguish the 
	 * different outputs.
	 */ 
	private String threadName;
	private String message;
	private String messageType;
	


	public ParsedInputOfUser(String destinationIp, int nVirtualMachine,
			int nMessage, int thinkTime, String inputStringFileName, String threadName, String message, String messageType ) {
		
		this.destinationIp = destinationIp;
		this.nVirtualMachine = nVirtualMachine;
		this.nMessage = nMessage;
		this.thinkTime = thinkTime;
		this.inputStringFileName = inputStringFileName;
		this.threadName = threadName;
		this.message = message;
		this.messageType = messageType;
	}

	public ParsedInputOfUser() {
	}

	public String getDestinationIp() {
		return destinationIp;
	}
	
	public String getDestinationPrivateIp(ServiceDirectory destination) {
		
		if(destinationIp.equals(PrivateIP.PRIVATE)){
			if(destination.equals(ServiceDirectory.UserBrowse))	destinationIp = PrivateIP.UserBrowse;
			if(destination.equals(ServiceDirectory.UserCart))	destinationIp = PrivateIP.UserCart;
			if(destination.equals(ServiceDirectory.UserPlaceOrder))	destinationIp = PrivateIP.UserPlaceOrder;
			if(destination.equals(ServiceDirectory.WebController))	destinationIp = PrivateIP.WebController;
			if(destination.equals(ServiceDirectory.ServiceLayer))	destinationIp = PrivateIP.ServiceLayer;
			if(destination.equals(ServiceDirectory.CartServer))	destinationIp = PrivateIP.CartServer;
			if(destination.equals(ServiceDirectory.CustomerServer))	destinationIp = PrivateIP.CustomerServer;
			if(destination.equals(ServiceDirectory.CatalogServer))	destinationIp = PrivateIP.CatalogServer;			
			if(destination.equals(ServiceDirectory.DeliveryOrderProc))	destinationIp = PrivateIP.DeliveryOrderProc;			
		}
		
		return destinationIp;
	}
	

	public void setDestinationIp(String destinationIp) {
		this.destinationIp = destinationIp;
	}

	public int getnVirtualMachine() {
		return nVirtualMachine;
	}

	public void setnVirtualMachine(int nVirtualMachine) {
		this.nVirtualMachine = nVirtualMachine;
	}

	public int getnMessage() {
		return nMessage;
	}

	public void setnMessage(int nMessage) {
		this.nMessage = nMessage;
	}

	public int getThinkTime() {
		return thinkTime;
	}

	public void setThinkTime(int thinkTime) {
		this.thinkTime = thinkTime;
	}

	public String getInputStringFileName() {
		return inputStringFileName;
	}

	public void setInputStringFileName(String inputStringFileName) {
		this.inputStringFileName = inputStringFileName;
	}

	public String getThreadName() {
		return threadName;
	}

	public void setThreadName(String threadName) {
		this.threadName = threadName;
	}		
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
}
