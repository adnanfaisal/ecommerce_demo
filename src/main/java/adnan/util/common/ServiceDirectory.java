package adnan.util.common;

public enum ServiceDirectory {

	UserBrowse, UserCart, UserPlaceOrder,
	WebController,
	ServiceLayer,
	CartServer, CustomerServer, CatalogServer, DeliveryOrderProc;
	
	public String toString(){
		
		switch(this){		
			case UserBrowse: return "UserBrowse";
			case UserCart: return "UserCart";
			case WebController: return "WebController";
			case ServiceLayer: return "ServiceLayer";
			case CartServer: return "CartServer";
			case CustomerServer: return "CustomerServer";
			case CatalogServer: return "CatalogServer";
			case DeliveryOrderProc: return "DeliveryOrderProc";
			default: return "";
		}
	}
}
