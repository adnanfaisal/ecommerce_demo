package adnan.util.common;

public class ParseInput {

	/**
	 * 
	 * @param args[0]: destinationIp ("private" for default private ip)
	 * @param args[1]: nVirtualMachine
	 * @param args[2]: nMessage
	 * @param args[3]: thinkTime
	 * @param args[4]: inputStringFileName if input message is to be read from a file (not implemented)
	 * @param args[5]: ThreadName
	 * @param args[5]: theMessage
	 * 
	 * @return
	 */
	public static ParsedInputOfUser parseInput(String[] args){
		return parseInput(args,"");
	}
	public static ParsedInputOfUser parseInput(String[] args, String messageType){
		
		ParsedInputOfUser parsedInput_User = new ParsedInputOfUser();
		try{
		if(args.length > 0)
			parsedInput_User.setDestinationIp(args[0]);
		if(args.length > 1)
			parsedInput_User.setnVirtualMachine(Integer.parseInt(args[1]));
	
		if(args.length > 2)
			parsedInput_User.setnMessage(Integer.parseInt(args[2]));
		
		if(args.length > 3)
			parsedInput_User.setThinkTime(Integer.parseInt(args[3]));
		 
		 if(args.length >  4)
			 parsedInput_User.setInputStringFileName(args[4]);
				
		  if(args.length >  5)
				 parsedInput_User.setThreadName(args[5]);
		  else
			  parsedInput_User.setThreadName("");
		  
		 
		  if(args.length >  6)
			   parsedInput_User.setMessage(ReadFile.readFile(args[6]));	
		  else
			  parsedInput_User.setMessage("");	
		  
		  parsedInput_User.setMessageType(messageType);
			  
		}
		catch (Exception ex){
			System.err.println("Parsing Error");
			ex.printStackTrace();
		}
				
		  
		return parsedInput_User;
		
	}
}
