package adnan.util.common;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class ReadFile {
	 
	public static String readFile(String fileName){
		  String theMessage = "";
		  File file = new File("files//" + fileName);
		  Scanner in;
		try {
			 in = new Scanner(file);
		     while (in.hasNextLine()){
			   theMessage += in.nextLine();
		  }
		  in.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
//		System.out.println(theMessage);
		return theMessage;
		  
	  }
}
