package adnan.util.common;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;


public class ConsumeCycle {


public  long consumeCycle(int nData, int nCycle){
	
	final long starttime = System.currentTimeMillis();
	int [] values = new int[nData];
	
	for(int k=0;k<nCycle;k++){
	
		Random r = new Random();
		for(int i = 0; i < nData; i++){
			values[i] = r.nextInt();
		}
	
		boolean swapped;
		do{
			swapped = false;
			for(int i=0;i<nData-1;i++){
				if(values[i]>values[i+1]){
					int temp = values[i];
					values[i] = values[i+1];
					values[i+1] = temp;
					swapped = true;
				}
			}
		}while(swapped);
	
	
	}
	final long endtime = System.currentTimeMillis();
	final long execTime = endtime-starttime;
//	System.out.print(execTime + " ");
	return execTime;
		
}

public void getConsumeStatistics(int nTimes, int nData, int nCycle){
	
	List<Double> consumeTimes = new LinkedList<Double>();
	
	System.out.println("\nAfter running consumeCycle " + nTimes + " times we get: ");
	for(int i=0;i<nTimes;i++ ){
		long x = consumeCycle(nData,nCycle);
		if( nTimes >=10 && i % (nTimes/10) == 0 )
			System.out.println("For i = " + i + " Last sample service demand : " + x);
		consumeTimes.add(1.0* x);
	}
	
	Statistics s = new Statistics(consumeTimes);
	System.out.println("\n" + s.toString());
	
}

public static void main(String args[]){

	// I suggest to run as: 
	// "java ConsumeCycle 1000 450 30"  - to get 30 ms service demand
	
	ConsumeCycle c = new ConsumeCycle();
	int nTimes = Integer.parseInt(args[0]); // no. of times you call consumeCycle
	int nData = Integer.parseInt(args[1]);  // no. of random elements in consumeCyle
	int nCycle = Integer.parseInt(args[2]); // no. of times random elements are generated
	
	
	c.getConsumeStatistics(nTimes, nData, nCycle); 
//	System.out.println(c.consumeCycle(400,30) + " ms");
	
	
//	System.out.println(consumeCycle(10000,1000) + " ms");
//	System.out.println(consumeCycle(10000,10000) + " ms");
	
}
	
}
