package adnan.util.common;

import java.io.Serializable;

public class Response implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long responseTime;
	private String message;
	
	public Response(){
		
		responseTime = 0L;
		message = "";		
	}
	public Response(long responseTime, String message) {
		this.responseTime = responseTime;
		this.message = message;
	}
	public long getResponseTime() {
		return responseTime;
	}
	public void setResponseTime(long responseTime) {
		this.responseTime = responseTime;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
}
