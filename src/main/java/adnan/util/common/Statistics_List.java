package adnan.util.common;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Statistics_List 
{
    private List<Double> data = new ArrayList<Double>();
    private String parameterName="";

    public  Statistics_List(List<Double> dataList){
    		this(dataList,"");    	 
    }
    
    public  Statistics_List(List<Double> dataList, String parameterName){
    	this.parameterName = parameterName;
    	this.data = dataList;
   	}
      
    private synchronized double getMean()
    {
        double sum = 0.0;
        if(data == null || data.size() == 0) return 0;
        for(double a : data)
            sum += a;
        return sum/data.size();
    }

    private synchronized double  getVariance()
    {
    	if(data == null || data.size() == 0) return 0;
    	
    	double mean = getMean();
        double temp = 0;
        
        for(double a :data)
            temp += (mean-a)*(mean-a);
        return temp/data.size();
    }

    private synchronized double  getStdDev()
    {
        return Math.sqrt(getVariance());
    }

    private synchronized double median() 
    {
       
    	if(data == null || data.size() == 0) return 0D;
    	
    	Collections.sort(data);

    	if (data.size() % 2 == 0) {
          return (data.get(data.size() / 2) - 1) + data.get(data.size() / 2) / 2.0;
       } 
       else {
          return data.get(data.size() / 2);
       }
    }
    
    @Override
    public synchronized String toString(){
    	
    	return this.parameterName + "(n = " + data.size() + ")" + " Mean = " + String.format("%.4f",getMean()) + " Median = " + String.format("%.4f",median()) + " Variance =  " + String.format("%.4f",getVariance()) + " StdDev = " + String.format("%.4f",getStdDev());   
    }
}