package adnan.util.common;

import java.rmi.RemoteException;
import java.util.List;

import adnan.rmi.util.RmiCall;

public class CallWrapper {
	  public  List<Double> makeACall(String inMessage, String destIp, List<Double> responseTimes, String serviceToCall,String messageType){
		  try {
				Double responseTime = 1.0 * new RmiCall().makeRmiCalls(destIp, 1, 1, inMessage, serviceToCall,messageType);
				responseTimes.add(responseTime);
				return responseTimes;
			  } catch (InterruptedException | RemoteException e) {
				e.printStackTrace();
			}
		  return responseTimes;
	  }
}
