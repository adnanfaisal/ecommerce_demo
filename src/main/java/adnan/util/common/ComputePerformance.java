package adnan.util.common;


public class ComputePerformance {

	public static Double getThroughput(long globalStartTime, long currentTimeMillis, long nMessage) {
		return ((1.0*nMessage) / (currentTimeMillis - globalStartTime));
	}
}
