package adnan.util.common;

import java.util.List;

public class DisplayOutput {

	 public  void showThroughput(long globalStartTime, long nReceivedMessage){
			  System.out.println("Throughput = " + ComputePerformance.getThroughput(globalStartTime,System.currentTimeMillis(),nReceivedMessage));	 
	  }
	 
	 public  void showMessageReceived(String whoAmI, String inMessage, long nReceivedMessage, String messageType){
			
		  // don't print the message if it is too long (as it will overload the console) or empty
		  if(!inMessage.isEmpty() && inMessage.length() < 100){
			  System.out.println("Message received " + inMessage);
		  }
		  
		  System.out.println("Message type " + messageType);

		  //	  System.out.println("service demand SAMPLE =  " + d + " ms.");
		  System.out.println("In " + whoAmI + " : nReceivedMessage = " + nReceivedMessage);
	  }
	 
	  public void showDemand(List<Double> demands){
		  if(!demands.isEmpty())
		  	System.out.println(new Statistics(demands).toString());
	  }
	  
	  public  void showResponseTime(List<Double> responseTimes){
		  if(!responseTimes.isEmpty())
			  System.out.println("R: " + new Statistics(responseTimes).toString());
		  System.out.println("");
	  }
}
