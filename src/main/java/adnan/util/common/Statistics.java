package adnan.util.common;

import java.util.Arrays;
import java.util.List;

public class Statistics 
{
    double[] data;   
    String parameterName="";

    public Statistics(double[] data, String parameterName){
    	  this.data = data;
          this.parameterName = parameterName;
    }
    public Statistics(double[] data) 
    {
      this(data,"");
    }   
    
    public Statistics(long[] data, String parameterName){
    	for(int i=0;i<data.length;i++)
        	this.data[i] = data[i] * 1.0;
        this.parameterName = parameterName;
  }
    public Statistics(long[] data) 
    {
        this(data,"");
    }
    
    
    public  Statistics(List<Double> dataList){
    		this(dataList,"");
    	 
    }
    
    public  Statistics(List<Double> dataList, String parameterName){
    	this.parameterName = parameterName;
    	data = new double[dataList.size()];
 //   	System.out.println(" size  = "  + dataList.size() );
    	
    	for (int i = 0; i < data.length; i++) {
//    	    System.out.print(" i=" + i );    		
//    		System.out.println(" dataList(i)=" + dataList.get(i));
    		data[i] = (Double) dataList.get(i);
    	 }
    	
    }
    

    double getMean()
    {
        double sum = 0.0;
        for(double a : data)
            sum += a;
        return sum/data.length;
    }

    double getVariance()
    {
        double mean = getMean();
        double temp = 0;
        for(double a :data)
            temp += (mean-a)*(mean-a);
        return temp/data.length;
    }

    double getStdDev()
    {
        return Math.sqrt(getVariance());
    }

    public double median() 
    {
       Arrays.sort(data);

       if (data.length % 2 == 0) 
       {
          return (data[(data.length / 2) - 1] + data[data.length / 2]) / 2.0;
       } 
       else 
       {
          return data[data.length / 2];
       }
    }
    
    public String toString(){
    	
    	return this.parameterName + " Mean = " + String.format("%.4f",getMean()) + " Median = " + String.format("%.4f",median()) + " Variance =  " + String.format("%.4f",getVariance()) + " StdDev = " + String.format("%.4f",getStdDev());   
    }
}