package adnan.util.common;

public class PrivateIP {
	
	// For subnet west-2b
	/*
	public static final String UserBrowse = "172.31.33.0";
	public static final String UserCart = "172.31.34.0";
	public static final String UserPlaceOrder = "172.31.35.0";
	
	public static final String WebController = "172.31.36.0";
	public static final String ServiceLayer = "172.31.37.0";

	public static final String CatalogServer = "172.31.38.0";
	public static final String CartServer = "172.31.39.0";
	public static final String CustomerServer = "172.31.40.0";
	public static final String DeliveryOrderProc = "172.31.41.0";

	public static final String PRIVATE = "private";
	public static final String LOCAL_HOST = "localhost";
	*/
	
	// For subnet west-2a
	
	public static final String UserBrowse = "172.31.17.0";
	public static final String UserCart = "172.31.18.0";
	public static final String UserPlaceOrder = "172.31.19.0";
	
	public static final String WebController = "172.31.20.0";
	public static final String ServiceLayer = "172.31.21.0";

	public static final String CatalogServer = "172.31.22.0";
	public static final String CartServer = "172.31.23.0";
	public static final String CustomerServer = "172.31.24.0";
	public static final String DeliveryOrderProc = "172.31.25.0";

	public static final String PRIVATE = "private";
	public static final String LOCAL_HOST = "localhost";
	
  
}
