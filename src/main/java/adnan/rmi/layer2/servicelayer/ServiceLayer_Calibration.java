package adnan.rmi.layer2.servicelayer;

/**
 * How to run: java Ecom3ServiceLayer clientIp6 7 8 9
 */

import java.rmi.*;

import adnan.util.common.PrivateIP;
import adnan.util.common.ServiceDirectory;
public class ServiceLayer_Calibration {
  public static void main(String args[]) {
    try {
    	String[] destIps = findDestIps(args);
    	ServiceLayerImpl_Calibration ecomServiceLayerImpl = new ServiceLayerImpl_Calibration(destIps);
    	String serviceName = ServiceDirectory.ServiceLayer.toString(); // name of this service 
    	Naming.rebind(serviceName, ecomServiceLayerImpl);
       	System.out.println("ServiceLayer is waiting for incoming message .... ");
       	}
    catch(Exception e) {
      System.out.println("Exception: " + e);
    }
  }
  
  public static String[] findDestIps(String[] args){
	  if(args.length > 0){
		  return args;
	  }	  
	  String []destIps = new String[4];
	  destIps[0] = PrivateIP.CatalogServer;
	  destIps[1] = PrivateIP.CartServer;
	  destIps[2] = PrivateIP.CustomerServer;
	  destIps[3] = PrivateIP.DeliveryOrderProc;
	  return destIps;   
  }
}