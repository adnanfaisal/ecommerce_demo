package adnan.rmi.layer2.servicelayer;

/**
 * How to run: java Ecom3ServiceLayer clientIp6 7 8 9
 */

import java.rmi.*;

import adnan.util.common.PrivateIP;
import adnan.util.common.ServiceDirectory;
public class ServiceLayer {
  public static void main(String args[]) {
    try {
    	String[] destIps = findDestIps(args);
    	System.out.println("DestIP " + destIps[0] + " " + destIps[1] + " " + destIps[2] + " " + destIps[3] + " ");
    	ServiceLayerImpl ecomServiceLayerImpl = new ServiceLayerImpl(destIps);
    	String serviceName = ServiceDirectory.ServiceLayer.toString(); //name of this service 
    	System.out.println("My service name " + serviceName);
    	
    	Naming.rebind(serviceName, ecomServiceLayerImpl);
       	System.out.println("ServiceLayer is waiting for incoming message .... ");
    }
    catch(Exception e) {
      System.out.println("Exception: " + e);
    }
  }
  
  
  public static String[] findDestIps(String[] args){
	  
	  // verify if he passed four "private", if no then return whatever ip he passed
	  if(args.length > 3){ 
		  if(! args[0].equals(PrivateIP.PRIVATE)) return args;
		  if(! args[1].equals(PrivateIP.PRIVATE)) return args;
		  if(! args[2].equals(PrivateIP.PRIVATE)) return args;
		  if(! args[3].equals(PrivateIP.PRIVATE)) return args;
		  
	  }	
	  
	  // if user passed no argument or passed four privates then return private ips
	  String []destIps = new String[4];
	  destIps[0] = PrivateIP.CatalogServer;
	  destIps[1] = PrivateIP.CartServer;
	  destIps[2] = PrivateIP.CustomerServer;
	  destIps[3] = PrivateIP.DeliveryOrderProc;
	  return destIps;   
  }
  
}