package adnan.rmi.layer2.servicelayer;


import java.rmi.*;
import java.rmi.server.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import adnan.rmi.util.RmiCall;
import adnan.rmi.util.RMIServerIntf;
import adnan.util.common.Constants;
import adnan.util.common.DisplayOutput;
import adnan.util.common.ConsumeCycle;
import adnan.util.common.Response;
import adnan.util.common.Response100KB;
import adnan.util.common.Response200KB;
import adnan.util.common.Response300KB;
import adnan.util.common.Response50KB;
import adnan.util.common.ServiceDirectory;

public class ServiceLayerImpl extends UnicastRemoteObject implements RMIServerIntf {
	  
	long nMessageTotal=0;
	long globalStartTime = 0;
	static long nReceivedMessage = 0;
	private List<String> destIps = new ArrayList<>();
	long filter = Constants.FILTER;
	
	List<Double> responseTimes = new ArrayList<>();
	List<Double> throughputs = new LinkedList<Double>();
	List<Double> demands = new LinkedList<Double>();
	
	
  public ServiceLayerImpl(String []destIp) throws RemoteException {
		super();
		destIps = Arrays.asList(destIp);
	}

/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public ServiceLayerImpl() throws RemoteException {
	}


	public Response doOperation(long totalMessage, String inMessage, String messageType) throws RemoteException {

	  

	  long a = System.currentTimeMillis();
	  if(globalStartTime == 0)
		  globalStartTime = a;
	  
	  nReceivedMessage++;
	  
	  
	  DisplayOutput displayOutput = new DisplayOutput();
	  if(nReceivedMessage % filter == 0)
	  {		 
		  displayOutput.showThroughput(globalStartTime,nReceivedMessage);
		  displayOutput.showMessageReceived( ServiceDirectory.ServiceLayer.toString(), inMessage, nReceivedMessage,messageType);
		  displayOutput.showDemand(demands);
		  displayOutput.showResponseTime(responseTimes);	
	  }
	  
	  executeOperation(inMessage,messageType);
	  

	  long b= System.currentTimeMillis();
	  return new Response50KB(b-a);
  }
  
  private void executeOperation(String inMessage, String messageType){
	  
	  // consume cycle
	  ConsumeCycle consumeCycle = new ConsumeCycle();

	    
	  // BROWSE 
	  if(messageType.equals("Browse")){	
		  long d = consumeCycle.consumeCycle(154, 75); // 4ms
		  if(nReceivedMessage % filter ==0)
			  System.out.println("d of browse = " + d);
		
		  // MAKE AN RPC CALL 	  
		  try {
			  Double responseTime =  1.0* new RmiCall().makeRmiCalls(destIps.get(0), 1, 1, inMessage, ServiceDirectory.CatalogServer.toString(),messageType);
			  responseTimes.add(responseTime);
		  } catch (InterruptedException | RemoteException e) {
				e.printStackTrace();
			}
		
	  }
		  
	  // CART
	  else if(messageType.equals("Cart")){
		  long d = consumeCycle.consumeCycle(154, 75); // 4ms
		  if(nReceivedMessage % filter ==0)
				  System.out.println("d of cart = " + d);
		  // MAKE AN RPC CALL 	  
		  try {
			Double responseTime = 1.0*new RmiCall().makeRmiCalls(destIps.get(1), 1, 1, inMessage, ServiceDirectory.CartServer.toString(),messageType);
			 responseTimes.add(responseTime);
		  } catch (InterruptedException | RemoteException e) {
			e.printStackTrace();
		}
	  }
	  
	  // PLACE ORDER 
	  else if(messageType.equals("PlaceOrder")){
		  
		  Random rand = new Random();
		  int number = rand.nextInt(34);
		  
		  // emulate the four calls coming out of placeOrder
		  for(int i=0;i<4;i++){
			// should consume 1/4th of the total demand of placeOrder
			  long d = consumeCycle.consumeCycle(154, 188); // 10 ms
			  if(nReceivedMessage % filter ==0)
				  System.out.println("d of 1/4 of placeOrder = " + d);
			  	//System.out.println(new Util2Statistics(demands).toString());	  				  
			  if(number >= 0 && number < 10){
				  
				  try {                                             // entry name, task name     
					Double responseTime = 1.0*new RmiCall().makeRmiCalls(destIps.get(1), 1, 1, "cartInfo", ServiceDirectory.CartServer.toString(),messageType);
					 responseTimes.add(responseTime);
				  } catch (InterruptedException | RemoteException e) {
					e.printStackTrace();
				}
			  }
			  else if(number >= 10 && number < 20){
					  
				  try {                                             // entry name, task name     
					Double responseTime = 1.0*new RmiCall().makeRmiCalls(destIps.get(1), 1, 1, "empty", ServiceDirectory.CartServer.toString(),messageType);
					 responseTimes.add(responseTime);
				  } catch (InterruptedException | RemoteException e) {
					e.printStackTrace();
				}
				
			  }
			  else if(number >= 20 && number < 24){
	  
				  try {                                             // entry name, task name     
					Double responseTime = 1.0* new RmiCall().makeRmiCalls(destIps.get(2), 1, 1, "custInfo", ServiceDirectory.CustomerServer.toString(),messageType);
					 responseTimes.add(responseTime);
				  } catch (InterruptedException | RemoteException e) {
					e.printStackTrace();
				}
		  }
			  else if(number >= 24 && number < 34){
				  
				  try {                                             // entry name, task name     
					Double responseTime = 1.0*new RmiCall().makeRmiCalls(destIps.get(3), 1, 1, "delivOrder", ServiceDirectory.DeliveryOrderProc.toString(),messageType);
					 responseTimes.add(responseTime);
				  } catch (InterruptedException | RemoteException e) {
					e.printStackTrace();
				}
	
		  }
	  }
	 } 	  
  }
 
}