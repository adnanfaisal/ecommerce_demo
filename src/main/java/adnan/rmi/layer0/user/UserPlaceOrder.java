package adnan.rmi.layer0.user;

/* 
 
@author Adnan
@version update type of user. Trying to emulate the model presented in dorina cascon 2016 paper */

import java.rmi.*;

import adnan.rmi.util.RmiCall;
import adnan.util.common.ParseInput;
import adnan.util.common.ParsedInputOfUser;
import adnan.util.common.PoissonProcess;
import adnan.util.common.ServiceDirectory;





public class UserPlaceOrder  {
	
	private static final String messageType = "PlaceOrder";
	/**
	 * usage: java <thisClass> destIp nVM nMessage thinkTime fileName/null
	 * @param args input parameters in the sequence: destIp nVM nMessage thinkTime fileName/null
	 */
	  public static void main(String args[]) {
	    
		  // parse input 
		  ParsedInputOfUser parsedInputOfUser = ParseInput.parseInput(args,messageType); 
		  PoissonProcess poisson = new PoissonProcess(parsedInputOfUser.getThinkTime());
		  
		  
		 // make calls 				
		  try {
			  	new RmiCall().makeRmiCalls(parsedInputOfUser.getDestinationPrivateIp(ServiceDirectory.WebController), parsedInputOfUser.getnVirtualMachine(), parsedInputOfUser.getnMessage(),
			  			parsedInputOfUser.getMessage(), ServiceDirectory.WebController.toString(),poisson,parsedInputOfUser.getThreadName(),true,messageType);   
					  
		  } catch (RemoteException | InterruptedException e) {
			  e.printStackTrace();
		  }		 
	  }
}