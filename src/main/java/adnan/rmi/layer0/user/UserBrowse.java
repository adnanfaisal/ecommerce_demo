package adnan.rmi.layer0.user;

/* 
 
@author Adnan
@version Read type of user. Trying to emulate the model presented in dorina cascon 2016 paper */

import java.rmi.RemoteException;

import adnan.rmi.util.RmiCall;
import adnan.util.common.*;


public class UserBrowse  {
	 
	private static final String messageType = "Browse";
	/**
	 * usage: java <thisClass> destIp nVM nMessage thinkTime fileName/null
	 * @param args input parameters in the sequence: destIp nVM nMessage thinkTime fileName/null
	 */

  public static void main(String args[]) {
    
	  // parse input 
	  ParsedInputOfUser parsedInputOfUser = ParseInput.parseInput(args,messageType); 
	  PoissonProcess poisson = new PoissonProcess(parsedInputOfUser.getThinkTime());
	 
	  
	 // make calls 				
	  try {
		  	
		  	String destIp = parsedInputOfUser.getDestinationPrivateIp(ServiceDirectory.WebController);
		  	System.out.println("DestIP = " + destIp);
		  	String serviceName = ServiceDirectory.WebController.toString();
		  	System.out.println("Calling Service name = " + serviceName);
		  	
		  	new RmiCall().makeRmiCalls(destIp, parsedInputOfUser.getnVirtualMachine(), parsedInputOfUser.getnMessage(),
		  			parsedInputOfUser.getMessage(),serviceName ,poisson,parsedInputOfUser.getThreadName(),true,messageType);   

//		  	new RmiCall().makeRmiCalls(parsedInputOfUser.getDestinationIp(), parsedInputOfUser.getnVirtualMachine(), parsedInputOfUser.getnMessage(),
//			parsedInputOfUser.getMessage(), ServiceDirectory.WebController.toString(),poisson,"",true,messageType);   

		  
	  } catch (RemoteException | InterruptedException e) {
		  e.printStackTrace();
	  }
	 
  }
 
}