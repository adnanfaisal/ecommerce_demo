package adnan.rmi.layer0.user;

/* 

This calibration class does the same works comparing to its actual class (i.e. class name without _Calibration). But, this version
is used for calibration in order to keep things separate. 

@author Adnan
@version Read type of user. Trying to emulate the model presented in dorina cascon 2016 paper 

*/


import java.rmi.*;

import adnan.rmi.util.RmiCall;
import adnan.util.common.ParseInput;
import adnan.util.common.ParsedInputOfUser;
import adnan.util.common.PoissonProcess;
import adnan.util.common.ServiceDirectory;




public class UserBrowse_Calibration  {
	
	private static final String messageType = "Browse";
	/**
	   * usage: java <thisClass> destIp nVM nMessage thinkTime fileName/null
	   * @param args input parameters in the sequence: destIp nVM nMessage thinkTime fileName/null
	   */
	  public static void main(String args[]) {
	    
		  // parse input 
		  ParsedInputOfUser parsedInputOfUser = ParseInput.parseInput(args,messageType); 
		  PoissonProcess poisson = new PoissonProcess(parsedInputOfUser.getThinkTime());
		  
		  
		 // make calls 				
		  try {
			  	new RmiCall().makeRmiCalls(parsedInputOfUser.getDestinationPrivateIp(ServiceDirectory.WebController), parsedInputOfUser.getnVirtualMachine(), parsedInputOfUser.getnMessage(),
			  			parsedInputOfUser.getMessage(), ServiceDirectory.WebController.toString(),poisson,parsedInputOfUser.getThreadName(),true,messageType);   
								  
		  } catch (RemoteException | InterruptedException e) {
			  e.printStackTrace();
		  }
		 
	  }

}