package adnan.rmi.util;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.util.LinkedList;
import java.util.List;

import adnan.util.common.*;


public class RmiCall {
	
	/**
	 * 
	 * @param destIp
	 * @param nThread
	 * @param nMessage
	 * @param theMessage
	 * @param serviceName
	 * @return
	 * @throws RemoteException
	 * @throws InterruptedException
	 */
	
	public long makeRmiCalls(String destIp, long nThread, long nMessage,String theMessage, String serviceName, String messageType) throws RemoteException, InterruptedException {
			return makeRmiCalls(destIp,nThread,nMessage,theMessage,serviceName, null,"", false, messageType);			
	}
	/**
	 * 
	 * @param destIp
	 * @param nThread
	 * @param nMessage
	 * @param theMessage
	 * @param serviceName Name of the service at the server
	 * @param poisson Pass null in case of no think time
	 * @param threadName Name of the thread, pass empty string if not needed
	 * @param print boolean, yes if you want to output the statistics of the calls
	 * @param messageType type of the message (e.g., Browse, Cart)
	 * @return long elapsed time in milliseconds
	 * @throws RemoteException
	 * @throws InterruptedException
	 */
	public long makeRmiCalls(String destIp, long nThread, long nMessage,String theMessage, String serviceName, PoissonProcess poisson, String threadName, boolean print, String messageType) throws RemoteException, InterruptedException {
		
		long tc = 0;
	    long td = 0;
	    
	    int count;
	    long totalMessages = nThread * nMessage;
	    long ta,tb;
	    long sDemand;
	    List<Double>  responseTimes = new LinkedList<Double>();
	    List<Double>  sDemands = new LinkedList<Double>();
	    RMIServerIntf rmiServerIntf = null;
	    
	    try {        
	        String rmiServerURL = "rmi://" + destIp + "/" + serviceName;
	        rmiServerIntf = (RMIServerIntf)Naming.lookup(rmiServerURL);      
	      }
	      catch (Exception c){
	      	c.printStackTrace();
	      }
	    
	    
	    long starttime = System.currentTimeMillis();
		  for(count = 0 ; count <nMessage; count ++){
			
			 
			  // emulate think time 			
			  if(poisson != null)
				  Thread.sleep(poisson.getArrivalIntervalMs());
			
			ta = System.currentTimeMillis();
			 sDemand = rmiServerIntf.doOperation(totalMessages,theMessage,messageType).getResponseTime();
//			System.out.println(" The sum is: " + addServerIntf.add(d1, d2));
			tb = System.currentTimeMillis();
			 tc = tb - ta;
			 td += tc;
			 responseTimes.add(1.0*tc);
			 sDemands.add(1.0*sDemand);
			 if(count % (Constants.FILTER ) == 0)
				{
				 	System.out.print("for count = " + count + " current R " + tc);
				 	System.out.println(", X " + (count * 1.0) / (tb - starttime));
				}
			 
		  }
		  long endtime = System.currentTimeMillis();
		  long elapsedTime = endtime - starttime;
		  
		  if(print){
		  Statistics st1 = new Statistics(responseTimes);
		  System.out.print("\n Thread Name : " + threadName + " Response times: " + st1.toString());
		  Statistics st2 = new Statistics(sDemands);
		  System.out.print("\n  Thread Name : " + threadName + " Service demand: " + st2.toString());
		  
		  System.out.println("\n Thread Name : " + threadName + " Mean Response time=" + (td*1.0/count) + " ms.");
		  System.out.println("\n Thread Name : " + threadName + " Throughput = " + count *1.0 / elapsedTime + "jobs/ms");	
		  }
		  return elapsedTime;
	}

}
