package adnan.rmi.util;

import java.rmi.*;

import adnan.util.common.Response;

/**
 * This is a class that is used by all the server implemenations,
 * no matter if the service has demand / no demand / encryption etc. 
 * @author Adnan
 * @version April 2016
 */
public interface RMIServerIntf extends Remote {
 // long add(long d1, long d2) throws RemoteException;
  Response doOperation(long totalMessage,String theMessage, String messageType) throws RemoteException;
}
