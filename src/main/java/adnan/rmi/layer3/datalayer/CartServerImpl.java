package adnan.rmi.layer3.datalayer;


import java.rmi.*;
import java.rmi.server.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import adnan.rmi.util.RMIServerIntf;
import adnan.util.common.Constants;
import adnan.util.common.DisplayOutput;
import adnan.util.common.ConsumeCycle;
import adnan.util.common.Response;
import adnan.util.common.Response100KB;
import adnan.util.common.Response300KB;
import adnan.util.common.ServiceDirectory;
public class CartServerImpl extends UnicastRemoteObject implements RMIServerIntf {
	  
	long nMessageTotal=0;
	long globalStartTime = 0;
	static long nReceivedMessage = 0;
	List<Double> throughputs = new LinkedList<Double>();
	long filter = Constants.FILTER;
	List<Double> responseTimes = new ArrayList<>();
	List<Double> demands = new ArrayList<>();
 
	
	private static final long serialVersionUID = 1L;
	public CartServerImpl() throws RemoteException {
  }

public Response doOperation(long totalMessage, String inMessage, String messageType) throws RemoteException {

	  long a = System.currentTimeMillis();
	  if(globalStartTime == 0)
		  globalStartTime = a;
	  nReceivedMessage++;
	    
	  // consume cycle
	  long d = new ConsumeCycle().consumeCycle(154, 150); // gives 8 ms in m4.large	  
	  demands.add(d*1.0); // typecast to double 

	  DisplayOutput displayOutput = new DisplayOutput();
	  if(nReceivedMessage % filter == 0)
	  {		 
		  displayOutput.showThroughput(globalStartTime,nReceivedMessage);
		  displayOutput.showMessageReceived(ServiceDirectory.CartServer.toString(), inMessage, nReceivedMessage,messageType);
		  displayOutput.showDemand(demands);
		  displayOutput.showResponseTime(responseTimes);	
	  }

	  long b= System.currentTimeMillis();
	  return new Response100KB(b-a);
	  
  }

}