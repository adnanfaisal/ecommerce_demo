package adnan.rmi.layer3.datalayer;

/**
 * How to run: java Ecom4CustomerServer
 */

import java.rmi.*;

import adnan.util.common.ServiceDirectory;
public class CustomerServer {
  public static void main(String args[]) {
    try {
    	CustomerServerImpl ecomCustomerServerImpl = new CustomerServerImpl();
    	String serviceName =  ServiceDirectory.CustomerServer.toString(); // e.g., my bind name
    	Naming.rebind(serviceName, ecomCustomerServerImpl);
    	System.out.println(serviceName + " is waiting for incoming message .... ");
    }
    catch(Exception e) {
      System.out.println("Exception: " + e);
    }
  }
}