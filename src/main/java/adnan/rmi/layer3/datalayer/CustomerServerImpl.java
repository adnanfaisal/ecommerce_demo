package adnan.rmi.layer3.datalayer;


import java.rmi.*;
import java.rmi.server.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import adnan.rmi.util.RMIServerIntf;
import adnan.util.common.Constants;
import adnan.util.common.DisplayOutput;
import adnan.util.common.ConsumeCycle;
import adnan.util.common.Response;
import adnan.util.common.Response100KB;
import adnan.util.common.ServiceDirectory;


public class CustomerServerImpl extends UnicastRemoteObject implements RMIServerIntf {
	  
	long nMessageTotal=0;
	long globalStartTime = 0;
	static long nReceivedMessage = 0;
	List<Double> throughputs = new LinkedList<Double>();
	List<Double> responseTimes = new ArrayList<>();
	long filter  = Constants.FILTER;
	List<Double> demands = new ArrayList<>();
  
	private static final long serialVersionUID = 1L;
	
	public CustomerServerImpl() throws RemoteException {
	}


	public Response doOperation(long totalMessage, String inMessage, String messageType) throws RemoteException {


		  long a = System.currentTimeMillis();
		  if(globalStartTime == 0)
			  globalStartTime = a;
		  nReceivedMessage++;
		    
		  // consume cycle
		  long d = new ConsumeCycle().consumeCycle(154, 75); // gives 4 ms	  
		  demands.add(d*1.0); // typecast to double 

		  DisplayOutput displayOutput = new DisplayOutput();
		  if(nReceivedMessage % filter == 0)
		  {		 
			  displayOutput.showThroughput(globalStartTime,nReceivedMessage);
			  displayOutput.showMessageReceived(ServiceDirectory.CustomerServer.toString(), inMessage, nReceivedMessage,messageType);
			  displayOutput.showDemand(demands);
			  displayOutput.showResponseTime(responseTimes);	
		  }

		  long b= System.currentTimeMillis();
		  return new Response100KB(b-a);
		  
  }
  

}