package adnan.rmi.layer3.datalayer;

/**
 * How to run: java Ecom2WebController
 */

import java.rmi.*;

import adnan.util.common.ServiceDirectory;
public class CartServer {
  public static void main(String args[]) {
    try {
    	CartServerImpl ecomCartServerImpl = new CartServerImpl();
    	String serviceName = ServiceDirectory.CartServer.toString() ; // e.g., my bind name
    	Naming.rebind(serviceName, ecomCartServerImpl);
    	System.out.println(serviceName + " is waiting for incoming message .... ");
    }
    catch(Exception e) {
      System.out.println("Exception: " + e);
    }
  }
}