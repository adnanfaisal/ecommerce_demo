package adnan.rmi.layer3.datalayer;

/**
 * How to run: java Ecom4CatalogServer
 */

import java.rmi.*;

import adnan.util.common.ServiceDirectory;
public class CatalogServer {
  public static void main(String args[]) {
    try {
    	CatalogServerImpl ecomCatalogServerImpl = new CatalogServerImpl();
    	String serviceName =  ServiceDirectory.CatalogServer.toString(); // e.g., my bind name
    	Naming.rebind(serviceName, ecomCatalogServerImpl);
    	System.out.println(serviceName + " is waiting for incoming message .... ");
    }
    catch(Exception e) {
      System.out.println("Exception: " + e);
    }
  }
}