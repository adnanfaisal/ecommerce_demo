package adnan.rmi.layer3.datalayer;

/**
 * How to run: java Ecom4DeliveryOrderProc
 */

import java.rmi.*;

import adnan.util.common.ServiceDirectory;
public class DeliveryOrderProc {
  public static void main(String args[]) {
    try {
    	DeliveryOrderProcImpl ecomDeliveryOrderProcImpl = new DeliveryOrderProcImpl();
    	String serviceName =  ServiceDirectory.DeliveryOrderProc.toString(); // e.g., my bind name
    	Naming.rebind(serviceName, ecomDeliveryOrderProcImpl);
    	System.out.println(serviceName + " is waiting for incoming message .... ");
    }
    catch(Exception e) {
      System.out.println("Exception: " + e);
    }
  }
}