package adnan.rmi.layer1.weblayer;

/**
 * How to run: java Ecom2WebController destIp
 */

import java.rmi.*;

import adnan.util.common.PrivateIP;
import adnan.util.common.ServiceDirectory;
public class WebController {
	
	 /**
	   * If you want to use private IP, don't pass any argument in the command line
	   * @param args
	   * @return
	   */
  public static void main(String args[]) {
    try {
    	String destIp = findDestIp(args);
    	System.out.println("DestIP " + destIp);
    	WebControllerImpl ecomWebControllerImpl = new WebControllerImpl(destIp);
    	String serviceName = ServiceDirectory.WebController.toString(); // name of this service 
    	System.out.println("My service name " + serviceName);
    	
    	Naming.rebind(serviceName, ecomWebControllerImpl);
    	System.out.println("WebController is waiting for incoming message .... ");
    }
    catch(Exception e) {
      System.out.println("Exception: " + e);
    }
  } 
 
  public static String findDestIp(String[] args){
	  if(args.length > 0){
		  if (args[0].equals(PrivateIP.LOCAL_HOST)) 
		  	return PrivateIP.LOCAL_HOST;
		  else if (args[0].equals(PrivateIP.PRIVATE)) 
			  	return PrivateIP.ServiceLayer;
		  return args[0]; // global ip
	  }	  
	  return PrivateIP.ServiceLayer;	// no ip was provided, so use private ip	  
  }
  
}