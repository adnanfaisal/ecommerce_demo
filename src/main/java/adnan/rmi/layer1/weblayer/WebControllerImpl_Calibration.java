package adnan.rmi.layer1.weblayer;


import java.rmi.*;
import java.rmi.server.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import adnan.rmi.util.RMIServerIntf;
import adnan.util.common.CallWrapper;
import adnan.util.common.Constants;
import adnan.util.common.DisplayOutput;
import adnan.util.common.Response;
import adnan.util.common.Response200KB;
import adnan.util.common.Response50KB;
import adnan.util.common.ServiceDirectory;

public class WebControllerImpl_Calibration extends UnicastRemoteObject implements RMIServerIntf {
	  
	long nMessageTotal=0;
	long globalStartTime = 0;
	static long nReceivedMessage = 0;
	List<Double> throughputs = new LinkedList<Double>();
	private String destIp;
	long filter=Constants.FILTER;
	List<Double> demands = new ArrayList<>();
	List<Double> responseTimes = new ArrayList<>();
	

	private static final long serialVersionUID = 1L;
	public WebControllerImpl_Calibration() throws RemoteException {
	}
	public WebControllerImpl_Calibration(String destIp) throws RemoteException {
		this.destIp = destIp;
	}

	public Response doOperation(long totalMessage, String inMessage, String messageType) throws RemoteException {

		 DisplayOutput displayOutput = new DisplayOutput();
		
		  long a = System.currentTimeMillis();
		  if(globalStartTime == 0)
			  globalStartTime = a;
		  nReceivedMessage++;
		  
		
		  
		  
		  // consume cycle
//		  long d = new ConsumeCycle().consumeCycle(120, 113); // 120, 113 give 4 ms in micro vm	  
//		  demands.add(d*1.0); // typecast to double 

		  if(nReceivedMessage % filter == 0)
		  {		  
			  displayOutput.showThroughput(globalStartTime,nReceivedMessage);
			  displayOutput.showMessageReceived(ServiceDirectory.WebController.toString(), inMessage, nReceivedMessage,messageType);
			  displayOutput.showDemand(demands);
			  displayOutput.showResponseTime(responseTimes);	
		  }
		 
		  String messageToSent="";
		  if(inMessage.length() < 100) 
			  messageToSent = inMessage;

		  responseTimes = new CallWrapper().makeACall(messageToSent,destIp,responseTimes,ServiceDirectory.ServiceLayer.toString(),messageType);
	 
		  long b= System.currentTimeMillis();
		  return new Response50KB(b-a);
  }

 
 
}