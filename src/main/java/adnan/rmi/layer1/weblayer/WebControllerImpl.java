package adnan.rmi.layer1.weblayer;


import java.rmi.*;
import java.rmi.server.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import adnan.rmi.util.RMIServerIntf;
import adnan.util.common.CallWrapper;
import adnan.util.common.Constants;
import adnan.util.common.DisplayOutput;
import adnan.util.common.ConsumeCycle;
import adnan.util.common.Response;
import adnan.util.common.Response100KB;
import adnan.util.common.Response200KB;
import adnan.util.common.Response300KB;
import adnan.util.common.Response50KB;
import adnan.util.common.ServiceDirectory;

public class WebControllerImpl extends UnicastRemoteObject implements RMIServerIntf {
	  

	private static final long serialVersionUID = 1L;
	long nMessageTotal=0;
	long globalStartTime = 0;
	static long nReceivedMessage = 0;
	
	private String destIp;
	
	
	long filter=Constants.FILTER;
	
	
	List<Double> demands = new ArrayList<>();
	List<Double> responseTimes = new ArrayList<>();
	List<Double> throughputs = new LinkedList<Double>();
	


	public WebControllerImpl() throws RemoteException {
	}

	public WebControllerImpl(String destIp) throws RemoteException {
			this.destIp = destIp;
	}

  public Response doOperation(long totalMessage, String inMessage, String messageType) throws RemoteException {

		  
	  long a = System.currentTimeMillis();
	  if(globalStartTime == 0)
		  globalStartTime = a;
	  nReceivedMessage++;
	    
	  // CONSUME CYCLE
	  long d = new ConsumeCycle().consumeCycle(154, 75); // 120, 113 give 4 ms in micro vm	  
	  demands.add(d*1.0); // typecast to double 

	  DisplayOutput displayOutput = new DisplayOutput();
	  if(nReceivedMessage % filter == 0)
	  {		 
		  displayOutput.showThroughput(globalStartTime,nReceivedMessage);
		  displayOutput.showMessageReceived(ServiceDirectory.WebController.toString(), inMessage, nReceivedMessage,messageType);
		  displayOutput.showDemand(demands);
		  displayOutput.showResponseTime(responseTimes);	
	  }
	 
	  String messageToSent="";
	  if(inMessage.length() < 100) 
		  messageToSent = inMessage;

	  // MAKE CALL
	  responseTimes = new CallWrapper().makeACall(messageToSent,destIp,responseTimes,ServiceDirectory.ServiceLayer.toString(), messageType);
 
	  long b= System.currentTimeMillis();
	  return new Response50KB(b-a);
  }
  
}